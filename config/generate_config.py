# coding: utf-8

import os

import random

PASSWD_SIZE = random.randrange(17, 20)
PASSWD_CHAR = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[(.!%$*:;)]'

random_password = ''
for i in range(PASSWD_SIZE):
    random_password += random.choice(PASSWD_CHAR)

PATH, fname = os.path.split(__file__)
with open(os.path.join(PATH, 'odoo.conf'), 'w') as conf:
    with open(os.path.join(PATH, 'odoo-template.conf'), 'r') as template:
        conf.write(template.read().replace('RANDOMIZE_ME', random_password))
